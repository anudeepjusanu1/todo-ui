angular.module('mod.m34', []);

angular.module('mod.m34')
  .constant('MOD_todo', {
        API_URL: 'http://localhost:3000/api/',
        API_URL_DEV: ''
    })
	.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('m34', {
                url: '/todo',
                parent: 'layout',
                template: "<div ui-view></div>",
                data: {
                    type: 'home'
                }
            })
            .state('m34.header', {
               url: '',
               template: "<div></div>",
               data: {
                   templateUrl:'mod_todo/views/menu/todo.header.html',
                   position: 1,
                   type: 'header',
                   name: "Profile",
                   module: "todo"
               }
           })
            /*.state('m34.dashboard', {
                url: '/dashboard',
                templateUrl: "mod_todo/views/dashboard/todo.dashboard.html",
                data: {
                    type: 'home',
                    menu: true,
                    name: "Dashboard",
                    module: "todo"
                }

            })*/
            
            .state('m34.todo', {
                url: '/todo',
                templateUrl: "mod_todo/views/todo/todoList.html",
                controller: "TodoController",
                data: {
                    type: 'home',
                    menu: true,
                    name: "Todo",
                    module: "todo"
                }

            })

            //FOR APP ADMIN
           /*.state('m34.admin', {
               url: '/admin',
               templateUrl: "mod_todo/views/admin/todo.admin.html",
               data: {
                   type: 'home',
                   menu: true,
                   admin : true,
                   disabled : false,
                   name: "Admin Page",
                   module: "todo"
               }
           })*/

    }])